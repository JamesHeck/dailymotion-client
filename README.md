Welcome to the Dailymotion-Client wiki!

* upload local videos
* upload remote videos
* whatch videos in (144-240-380-480-720)
* generate direct mp4 video links [(Video)](https://www.dailymotion.com/video/x7bdcfq)
* share videos
* edit video settings
* add video tags
* delete video

![https://i.postimg.cc/fLvcqg9L/ure-Wiz292.png](https://i.postimg.cc/fLvcqg9L/ure-Wiz292.png)

![https://i.postimg.cc/fyJV7H71/ure-Wiz293.png](https://i.postimg.cc/fyJV7H71/ure-Wiz293.png)

![https://i.postimg.cc/bwzs3NRQ/ure-Wiz294.png](https://i.postimg.cc/bwzs3NRQ/ure-Wiz294.png)

![https://i.postimg.cc/5ySfCLTD/ure-Wiz295.png](https://i.postimg.cc/5ySfCLTD/ure-Wiz295.png)

![https://i.postimg.cc/Hx3S8L3B/ure-Wiz296.png](https://i.postimg.cc/Hx3S8L3B/ure-Wiz296.png)

![https://i.postimg.cc/XvkLtJM4/ure-Wiz297.png](https://i.postimg.cc/XvkLtJM4/ure-Wiz297.png)

![https://i.postimg.cc/C5KPXxvy/Dailymotion-Client-rohym4-QYi-D.png](https://i.postimg.cc/C5KPXxvy/Dailymotion-Client-rohym4-QYi-D.png)

![https://i.postimg.cc/Vvnq7SML/Dailymotion-Client-a-Hg-EWff-Oq8.png](https://i.postimg.cc/Vvnq7SML/Dailymotion-Client-a-Hg-EWff-Oq8.png)